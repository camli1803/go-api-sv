package repositories

import (
	"go-api-sv/internal/pkg/domains/interfaces"
	"go-api-sv/internal/pkg/domains/models/entities"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type userRepository struct {
	logger *logrus.Logger
	DBConn *gorm.DB
}

func NewUserRepository(dbConn *gorm.DB) interfaces.UserRepository {
	return &userRepository{
		DBConn: dbConn,
	}
}

func (u *userRepository) Find() ([]entities.User, error) {
	users := []entities.User{}

	result := u.DBConn.Find(&users)

	return users, result.Error
}

func (u *userRepository) TakeByID(id uint) (entities.User, error) {
	user := entities.User{}
	result := u.DBConn.Take(&user, id)
	return user, result.Error
}

func (u *userRepository) TakeByUsername() (entities.User, error) {
	user := entities.User{}
	return user, nil
}

func (u *userRepository) TakeByEmail() (entities.User, error) {
	user := entities.User{}
	return user, nil
}

func (u *userRepository) Create(user entities.User) (entities.User, error) {
	result := u.DBConn.Create(&user)
	return user, result.Error
}
