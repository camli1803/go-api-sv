package migrations

import (
	"go-api-sv/internal/pkg/domains/models/entities"

	"gorm.io/gorm"
)

func Migrate(dbConn *gorm.DB) error {
	err := dbConn.AutoMigrate(entities.User{})

	return err
}
